" impowed status line
Plugin 'bling/vim-airline'
" fast fs tree navigation
Plugin 'scrooloose/nerdtree'
" buffer manipulation
Plugin 'fholgado/minibufexpl.vim'
" autocomplete for python
" Plugin 'davidhalter/jedi-vim'
" error checking
Plugin 'scrooloose/syntastic'
" snippets plugin
Plugin 'SirVer/ultisnips'
" Snippets are separated from the plugin.
Plugin 'honza/vim-snippets'
" plugin for mrporc syntax hightlighter (uncomment it on statbox-dev machine)
" Plugin 'git://github.yandex-team.ru/borman/vim-mrproc'


" ===================== code-style section ====================================
" easy comment for common languages
Plugin 'tpope/vim-commentary'
" python pep8 auto indent
Plugin 'hynek/vim-python-pep8-indent'


" =============================================================================
" =============================== Syntastic Settings ==========================
" =============================================================================

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_python_checkers = ['flake8', 'pylint', 'pep8']
let g:syntastic_python_pylint_post_args='--disable=C0103,C0111'

" =============================================================================
" ============================= UltiSnips Settings ============================
" =============================================================================

" =============================================================================
" ============================ MiniBufExpl Settings ===========================
" =============================================================================

let g:did_minibufexplorer_syntax_inits = 1
hi def link MBENormal                Comment
hi def link MBEChanged               String
hi def link MBEVisibleNormal         Type
hi def link MBEVisibleChanged        PreProc
hi def link MBEVisibleActiveNormal   Identifier
hi def link MBEVisibleActiveChanged  Statement

" =============================================================================
" ============================== FileType Settings ============================
" =============================================================================
autocmd BufRead,BufNewFile job* set filetype=mrproc

" =============================================================================
" ================================= Global Settings ===========================
" =============================================================================
let g:load_doxygen_syntax = 1  " For doxygen syntax highlight
let g:python_highlight_all = 1 " For full python syntax highlight

syntax on           " set syntax support if there's syntax file
set fdm=indent      " folding based on indent lvl
set ruler			" show the cursor position all the time
set number			" Show line numbers
set laststatus=2	" Always show the statusline
set cursorline      " Highlight current line
set colorcolumn=80  " color 80 characters in line

scriptencoding utf-8
set encoding=utf-8
set fileencodings=utf-8,cp1251,koi8-r

set list listchars=tab:»·,trail:·,extends:$,nbsp:=

set viminfo='20,\"50  " read/write file .viminfo, store<=50 lines of registers
set history=50        " keep 50 lines of command line history
" Suffixes that get lower priority when doing tab completion for filenames.
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,
            \.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc

set hidden      " more native buffer behavior
set showcmd		" Show (partial) command in status line.
set showmatch	" Show matching brackets.
set ignorecase	" Do case insensitive matching
set incsearch	" Incremental search
"set autowrite	" Automatically save before commands like :next and :make

set wildignore=*/.git/*,*/.svn/*
set wildignore+=*.o,*.so,*.pyc
set wildmode=longest:full
set wildmenu

set shiftwidth=4
set tabstop=4
set softtabstop=4
set smartindent		" always set autoindenting on
set smarttab
set expandtab


set hlsearch	" Highlight search
set nowrapscan	" Don't wrap around EOF or BOF while searching
set suffixesadd=.pl,.pm,.yml,.yaml,.tyaml " for `gf' (open file under cursor)





set backspace=indent,eol,start	" more powerful backspacing
set dir=~/.vim/swap_files   " district directory for all swap files
set backupcopy=yes " keep a backup file
if has("persistent_undo")
    set undofile    " Persistent  undo
    set undodir=~/.vim/undo
endif
