#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import print_function
import shutil
import os
import sys
import argparse
import subprocess as sb

# external update script required
DEFAULT_DOT_FILES_PATH = os.path.expanduser(os.path.join('~', '.dot_files'))
DOT_FILES_CLONE_COMMAND = "git clone https://bitbucket.org/svetlov/dot_files"
VIMRC_PATH = os.path.expanduser(os.path.join('~', '.vimrc'))
VIM_DIR_PATH = os.path.expanduser(os.path.join('~', '.vim'))
VUNDLE_INSTALL_COMMAND = (
    'git clone https://github.com/gmarik/Vundle.vim.git '
    + os.path.expanduser(os.path.join('~', '.vim', 'bundle', 'Vundle.vim'))
)
INSTALL_PLUGINS_COMMAND = 'vim -e +PluginInstall +qall'

VIMRC_FILE = {
    'adhoc': 'vimrc_adhoc',
    'dev': 'vimrc_dev'
}

def safe_shell_call(command, ignore_errors=False):
    print("Executing command %s" % command)
    command_handler = sb.Popen(command.split(), stderr=sb.PIPE)
    _, stderr = command_handler.communicate()
    if command_handler.returncode != 0 and not ignore_errors:
        print ("Error while executing '''%s''':" % command)
        print (stderr, end='')
        sys.exit(command_handler.returncode)


def get_free_filename(path):
    if os.path.lexists(path):
        counter = 1
        temp_path = path + '({0})'.format(counter)
        while os.path.lexists(temp_path):
            counter += 1
            temp_path = path + '({0})'.format(counter)
        path = temp_path

    return path


def process_old_configuration(force_delete_old):
    if not os.path.lexists(VIMRC_PATH) and not os.path.lexists(VIM_DIR_PATH):
        return []

    def backup_vimrc_configuration():
        backuped = []
        if os.path.lexists(VIMRC_PATH):
            vimrc_back = os.path.expanduser(os.path.join('~', '.vimrc_back'))
            vimrc_back = get_free_filename(vimrc_back)
            shutil.move(VIMRC_PATH, vimrc_back)
            backuped.append(vimrc_back)
        if os.path.lexists(VIM_DIR_PATH):
            vimdir_back = os.path.expanduser(os.path.join('~', '.vim_back'))
            vimdir_back = get_free_filename(vimdir_back)
            shutil.move(VIM_DIR_PATH, vimdir_back)
            backuped.append(vimdir_back)
        print(backuped)
        return backuped

    if force_delete_old:
        return backup_vimrc_configuration()

    while True:
        answer = raw_input(
            ("Warning: %s file exists. Override it?\n"
             "[Y]es\t[N]o\t[B]ackup old configuration: ") % VIMRC_PATH
        )
        if answer in ('y', 'Y', 'b', 'B'):
            backuped_files = backup_vimrc_configuration()
            if answer in ('y', 'Y'):
                return backuped_files
            else:
                return []
        elif answer in ('n', 'N'):
            print ("Denied to override %s" % VIMRC_PATH, file=sys.stderr)
            print ("Installation aborted by user", file=sys.stderr)
            sys.exit(1)


def clone_dot_files(dest_path, silent_mode, use_links):
    remove_targets = []
    if os.path.lexists(dest_path):
        if silent_mode:
            print ("%s exists, installations aborted" % dest_path)
            sys.exit(1)
        while True:
            answer = raw_input(
                ("Warning: %s directory exists. What to do?\n"
                 "[D]elete it\t[A]bort installation\t[U]se it: " % dest_path)
            )
            if answer in ('d', 'D'):
                dest_back = get_free_filename(dest_path)
                shutil.move(dest_path, dest_back)
                remove_targets.append(dest_back)
                safe_shell_call(DOT_FILES_CLONE_COMMAND + " " + dest_path)
                if not use_links:
                    remove_targets.append(dest_path)
                break
            elif answer in ('u', 'U'):
                break
            elif answer in ('a', 'A'):
                print ("Installation aborted", file=sys.stderr)
                sys.exit(1)
    else:
        safe_shell_call(DOT_FILES_CLONE_COMMAND + " " + dest_path)
    return remove_targets


def install_vimrc_file(dot_files_directory, use_links, mode):
    src = os.path.join(dot_files_directory, 'vim', VIMRC_FILE[mode])
    if not os.path.lexists(src):
        print(("%s not exists, please check your dot_files"
               " installation" % src) , file=sys.stderr)
        print("Installation aborted", file=sys.stderr)
        sys.exit(1)
    if use_links:
        os.symlink(src, VIMRC_PATH)
    else:
        shutil.copy(src, VIMRC_PATH)


def build_plugins(plugins):
    pass # TODO FIXME


def vimrc_postinstall():
    os.mkdir(os.path.expanduser(os.path.join('~', '.vim', 'undo')))
    os.mkdir(os.path.expanduser(os.path.join('~', '.vim', 'swap_files')))


def install_vim_configuration(params):
    required_to_delete = process_old_configuration(params.silent)
    required_to_delete.extend(clone_dot_files(params.dot_path, params.silent,
                                              params.uselinks))
    install_vimrc_file(params.dot_path, params.uselinks, params.mode)
    safe_shell_call(VUNDLE_INSTALL_COMMAND)
    # print("Vundle installed, trying to install other plugins")
    safe_shell_call(INSTALL_PLUGINS_COMMAND, ignore_errors=True)
    # build_plugins(plugins)
    vimrc_postinstall()

    for filepath in required_to_delete:
        while True:
            answer = raw_input("Do you wanna delete %s? (y/n) :" % filepath)
            if answer in ('Y', 'y'):
                safe_shell_call('rm -rf %s' % filepath)
                break
            elif answer in ('N', 'n'):
                break

def _create_parser():
    parser_ = argparse.ArgumentParser(description='Install vim files')
    parser_.add_argument('mode', type=str, choices=['adhoc', 'dev'])
    parser_.add_argument('dot_path', type=str, nargs='?', default=DEFAULT_DOT_FILES_PATH)
    parser_.add_argument('--uselinks', '-l', action='store_true')
    parser_.add_argument('--silent', '-s', action='store_true')

    return parser_


if __name__ == '__main__':
    parser = _create_parser()
    args = parser.parse_args()
    install_vim_configuration(args)
