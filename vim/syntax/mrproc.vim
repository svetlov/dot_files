" Vim syntax file
" Language: MrProc script
" Maintainer: Mikhail Borisov <borman@yandex-team.ru>

if exists("b:current_syntax")
	finish
endif

syn keyword mrActionMR map reduce sort drop read write copy contained
syn keyword mrAction publish export format fold[ing] sendmail serialize deserialize exists filecopy rm contained
syn keyword mrXRuleName priority run_at authors synchro_paths static_deps dynamic_deps scale dictionary contained

syn region mrSensible start=/^/ end=/$/ skip=/\\$/ contains=mrCommand,mrAssignment keepend
syn region mrComment start=/^\s*#/ end=/$/ skip=/\\$/
syn region mrXRule start=/^\s*#\s*\(priority\|run_at\|authors\|synchro_paths\|static_deps\|dynamic_deps\|scale\|dictionary\)\s*:/ end=/$/ skip=/\\$/ contains=mrXRuleName

syn case ignore
syn region mrImportantComment start=/^\s*#[^!].*\(!\|внимание\|warning\|attention\|beware\|NOTE\|TODO\|FIXME\|XXX\).*/ end=/$/ skip=/\\$/ contains=mrImportantWord
syn keyword mrImportantWord внимание warning attention beware NOTE TODO FIXME XXX contained
syn case match

syn match mrCommand /^\s*\w\+\>/ contained contains=mrAction,mrActionMR nextgroup=mrArgs
syn region mrArgs start=/./ end=/$/ skip=/\\$/ contained contains=mrSwitch,@mrValue,mrContinuation,mrBadContinuation

syn match mrAssignment /^\s*\w\+\s*=/ contained contains=mrLHS nextgroup=mrRHS
syn match mrLHS /\w\+/ contained nextgroup=mrEqOp skipwhite
syn match mrEqOp /=/ contained nextgroup=mrRHS skipwhite
syn match mrRHS /.*/ contained contains=@mrValue,mrContinuation,mrBadContinuation

syn cluster mrValue contains=mrBareword,mrString,mrMacro,mrVarRef

syn match mrBareword /\(\\$\|-\|"\|\$\)\@![^"{}$[:space:]]\+/ contained
syn region mrString start=/"/ end=/"/ skip=/\\$/ contained contains=mrMacro,mrVarRef
syn match mrSwitch /-\S\+/ contained
syn region mrMacro start=/\${/ end=/}/ contained contains=@mrValue,mrMacroName
syn match mrVarRef /\$\w\+/ contained
syn match mrVarRef /\$\w\+\[[0-9\-]\+\]/ contained contains=mrVarIndex

syn match mrVarIndex /\[[0-9\-]\+\]/ms=s+1,me=e-1 contained

syn match mrMacroName /{\s*\w\+\s/ms=s+1,me=e-1 contained

syn match mrBadContinuation /\\\s\+/ contained
syn match mrContinuation /\\\n/ contained

syn sync fromstart

" Apply highlighting
let b:current_syntax = "mrproc"

hi def link mrCommand Error

hi def link mrXRule Macro
hi def link mrXRuleName SpecialComment

hi def link mrComment Comment
hi def link mrImportantComment SpecialComment
hi def link mrImportantWord Todo

hi def link mrContinuation Comment
hi def link mrBadContinuation Error

hi def link mrLHS Identifier
hi def link mrEqOp Operator

hi def link mrActionMR Operator
hi def link mrAction Operator

hi def link mrString String
hi def link mrBareword String
hi def link mrSwitch Type

hi def link mrVarRef Identifier
hi def link mrVarIndex Number

hi def link mrMacro Identifier
hi def link mrMacroName Operator
hi def link mrMacroArg String
